# Equation Solver

## What it does?

You give it a json file which denotes an algebric equation with only one unknown variable on the left hand side and a constant on right hand side, it will simplify the equation and solve it for you.

## How to run?

1.  Clone this repository.

2.  Replace the equation in `./equation.js` with your equation JSON.
3.  Run `node index.js`.

OR

2.  Create a json file in the project root with the required equation json.
3.  Run `node index.js <filepath>`. Eg: `node index.js input.json`

The script would print the result to console.

## Note

1.  x should be present only at LHS
2.  x should appear only once. It means you cannot do `x + x` or `x + ( 2 * x )`.
3.  Equation should be linear.
4.  `op` should be one of `add`, `subtract`, `multiply` or `divide`.

## How do you contact me?

Mobile: +91-8877073355 or +91-8884785135  
Email: jibinmathews7@gmail.com  
Website: [http://www.jibinmathews.in](http://www.jibinmathews.in)
