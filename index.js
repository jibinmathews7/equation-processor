'use strict';

const fs = require('fs');
const path = require('path');

const lib = require('./lib');

let input = require('./equation');

if (process.argv[2]) {
  input = require(path.join(__dirname, process.argv[2]));
}

const simplifiedEquation = lib.simplifyEquation(input);

console.log('Initial Equation ', lib.jsonToEquation(input));
console.log('Simplified Equation ', lib.jsonToEquation(simplifiedEquation));
console.log('=> x = ', lib.evaluate(simplifiedEquation));
