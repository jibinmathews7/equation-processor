'use strict';

const { invereOpMapping } = require('./helpers/mappings');

module.exports = function(json) {
  const simplifiedJSON = {
    op: 'equal',
    lhs: 'x',
    rhs: json.rhs,
  };

  function doesContainX(object) {
    if (typeof object !== 'object' && object === 'x') {
      return true;
    }
    if (object && object.lhs && object.rhs) {
      return doesContainX(object.lhs) || doesContainX(object.rhs);
    }
    return false;
  }

  function processSide(object, parent) {
    if (!object) {
      return;
    }
    const originalOp = object.op;
    const inverseOp = invereOpMapping[originalOp];

    const prevRhs = JSON.parse(JSON.stringify(parent.rhs));

    parent.rhs = {};
    parent.rhs.op = inverseOp;
    parent.rhs.lhs = prevRhs;
    let toBeEvaluatedObject;
    if (doesContainX(object.lhs)) {
      // LHS has x. So move local rhs to rhs
      parent.rhs.rhs = object.rhs;
      toBeEvaluatedObject = object.lhs;
    } else {
      // RHS has x. So move local lhs to rhs
      parent.rhs.rhs = object.lhs;
      toBeEvaluatedObject = object.rhs;
    }

    processSide(toBeEvaluatedObject, parent);
  }

  processSide(json.lhs, simplifiedJSON);

  simplifiedJSON.rhs = simplifiedJSON.rhs.lhs;

  return simplifiedJSON;
};
