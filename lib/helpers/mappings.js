'use strict';

exports.opMapping = {
  add: '+',
  subtract: '-',
  multiply: '*',
  divide: '/',
  equal: '=',
};

exports.invereOpMapping = {
  add: 'subtract',
  subtract: 'add',
  multiply: 'divide',
  divide: 'multiply',
  equal: 'equal',
};

exports.functionMapping = {
  add: (a, b) => a + b,
  subtract: (a, b) => a - b,
  multiply: (a, b) => a * b,
  divide: (a, b) => a / b,
};
