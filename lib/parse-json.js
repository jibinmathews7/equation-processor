'use strict';

const { opMapping } = require('./helpers/mappings');

function processEq(sideDefinition, depth = 0) {
  const op = opMapping[sideDefinition.op];
  let lhsString, rhsString;
  if (typeof sideDefinition.lhs !== 'object') {
    lhsString = sideDefinition.lhs;
  } else {
    lhsString = processEq(sideDefinition.lhs, depth + 1);
  }
  if (typeof sideDefinition.rhs !== 'object') {
    rhsString = sideDefinition.rhs;
  } else {
    rhsString = processEq(sideDefinition.rhs, depth + 1);
  }
  return `${depth > 1 ? '(' : ''}${lhsString} ${op} ${rhsString}${depth > 1 ? ')' : ''}`;
}

module.exports = function(json) {
  return processEq(json);
};
