'use strict';

const { functionMapping } = require('./helpers/mappings');

module.exports = function(json) {
  const { rhs } = json;

  function processJSON(object) {
    if (typeof object !== 'object') {
      return object;
    }

    const lhsValue = processJSON(object.lhs);
    const rhsValue = processJSON(object.rhs);
    return functionMapping[object.op](lhsValue, rhsValue);
  }

  return processJSON(rhs);
};
