'use strict';

module.exports = {
  op: 'equal',
  lhs: {
    op: 'add',
    lhs: 1,
    rhs: {
      op: 'multiply',
      lhs: {
        op: 'add',
        lhs: 24,
        rhs: 'x',
      },
      rhs: {
        op: "divide",
        lhs: 10,
        rhs: {
          op: 'add',
          lhs: {
            op: 'multiply',
            lhs: 4,
            rhs: 10
          },
          rhs: 45
        }
      },
    },
  },
  rhs: 20,
};
